-- Uso de la base de datos (TCL)
USE practica_sql;


--EJERCICIOS

--Iniciar una transacción, realizar operaciones y luego confirmar los cambios:

BEGIN;
UPDATE usuarios
SET edad = edad + 1
WHERE id = 2;

DELETE FROM productos
WHERE cantidad < 10;

COMMIT;
