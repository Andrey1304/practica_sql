-- Creación de la base de datos (DDL)
CREATE DATABASE practica_sql;

-- Creación de la tabla 'usuarios' (DDL)
CREATE TABLE usuarios (
    id INT AUTO_INCREMENT PRIMARY KEY, -- DDL
    nombre VARCHAR(50), -- DDL
    email VARCHAR(50), -- DDL
    edad INT -- DDL
);

-- Creación de la tabla 'productos' (DDL)
CREATE TABLE productos (
    id INT AUTO_INCREMENT PRIMARY KEY, -- DDL
    nombre VARCHAR(50), -- DDL
    precio DECIMAL(10,2), -- DDL
    cantidad INT -- DDL
);

-- Creación de la tabla 'ordenes' (DDL)
CREATE TABLE ordenes (
    id INT AUTO_INCREMENT PRIMARY KEY, -- DDL
    fecha DATE, -- DDL
    usuario_id INT, -- DDL
    producto_id INT, -- DDL
    FOREIGN KEY (usuario_id) REFERENCES usuarios(id), -- DDL
    FOREIGN KEY (producto_id) REFERENCES productos(id) -- DDL
);

--EJERCICIOS

--Agregar una columna a la tabla 'usuarios':
ALTER TABLE usuarios
ADD COLUMN direccion VARCHAR(100);
