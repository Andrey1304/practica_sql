-- Inserción de datos en la tabla 'usuarios' (DML)
INSERT INTO usuarios (nombre, email, edad) VALUES
    ('Juan', 'juan@example.com', 25), -- DML
    ('María', 'maria@example.com', 30), -- DML
    ('Pedro', 'pedro@example.com', 28); -- DML

-- Inserción de datos en la tabla 'productos' (DML)
INSERT INTO productos (nombre, precio, cantidad) VALUES
    ('Camiseta', 19.99, 50), -- DML
    ('Pantalón', 39.99, 30), -- DML
    ('Zapatos', 59.99, 20); -- DML

-- Inserción de datos en la tabla 'ordenes' (DML)
INSERT INTO ordenes (fecha, usuario_id, producto_id) VALUES
    ('2022-01-01', 1, 1), -- DML
    ('2022-01-02', 2, 3), -- DML
    ('2022-01-03', 3, 2); -- DML

-- Consulta de datos en la tabla 'usuarios' (DML)
SELECT * FROM usuarios; -- DML

-- Consulta de datos en la tabla 'productos' (DML)
SELECT * FROM productos; -- DML

-- Consulta de datos en la tabla 'ordenes' con información relacionada (DML)
SELECT o.id, o.fecha, u.nombre AS nombre_usuario, p.nombre AS nombre_producto
FROM ordenes o
JOIN usuarios u ON o.usuario_id = u.id
JOIN productos p ON o.producto_id = p.id; -- DML

--EJERCICIOS

--Actualizar el precio de un producto en la tabla 'productos':
UPDATE productos
SET precio = 49.99
WHERE id = 1;
