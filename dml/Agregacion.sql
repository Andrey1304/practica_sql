--Calcula la suma de los precios de todos los productos en la tabla productos.
SELECT SUM(precio) AS total_precios
FROM productos;

--Obtén el promedio de edades de todos los usuarios en la tabla usuarios.
SELECT AVG(edad) AS promedio_edades
FROM usuarios;

--Encuentra el número máximo de unidades disponibles en la tabla productos.
SELECT MAX(cantidad) AS max_unidades
FROM productos;

--Calcula la suma total de ventas realizadas en la tabla ordenes.
SELECT SUM(precio * cantidad) AS total_ventas
FROM ordenes o
JOIN productos p ON o.producto_id = p.id;

--Obtiene el número total de usuarios registrados en la tabla usuarios.
SELECT COUNT(*) AS total_usuarios
FROM usuarios;
